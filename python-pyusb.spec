%global _empty_manifest_terminate_build 0
Name:		python-pyusb
Version:	1.2.1
Release:	2
Summary:	Python USB access module
License:	BSD-3-Clause
URL:		https://pyusb.github.io/pyusb
Source0:	https://files.pythonhosted.org/packages/d9/6e/433a5614132576289b8643fe598dd5d51b16e130fd591564be952e15bb45/pyusb-1.2.1.tar.gz
BuildArch:	noarch
BuildRequires:  python3-setuptools_scm

%description
PyUSB offers easy USB devices communication in Python.
It should work without additional code in any environment with
Python >= 3.6, ctypes and a pre-built USB backend library
(currently: libusb 1.x, libusb 0.1.x or OpenUSB).

%package -n python3-pyusb
Summary:	Python USB access module
Provides:	python-pyusb
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools

%description -n python3-pyusb
PyUSB offers easy USB devices communication in Python.
It should work without additional code in any environment with
Python >= 3.6, ctypes and a pre-built USB backend library
(currently: libusb 1.x, libusb 0.1.x or OpenUSB).

%package help
Summary:	Development documents and examples for pyusb
Provides:	python3-pyusb-doc

%description help
PyUSB offers easy USB devices communication in Python.
It should work without additional code in any environment with
Python >= 3.6, ctypes and a pre-built USB backend library
(currently: libusb 1.x, libusb 0.1.x or OpenUSB).

%prep
%autosetup -n pyusb-1.2.1

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-pyusb -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Wed May 11 2022 xigaoxinyan <xigaoxinyan@h-partners.com> -1.2.1-2
- License compliance rectification

* Wed Sep 01 2021 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
